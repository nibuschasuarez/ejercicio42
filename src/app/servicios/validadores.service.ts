import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {

  constructor() { }

  noZaida(control:FormControl):{[s: string]: boolean}{

    console.log(control.value);
    if(control.value?.toLowerCase()=== 'zaida'){
      return{
        noZaida:true
      }
    }
    return null as any;
  }

  noEveneser(control:FormControl):{[s: string]: boolean}{
    
    console.log(control.value);
    if(control.value?.toLowerCase()=== 'eveneser'){
      return{
        noEveneser:true
      }
    }
    return null as any;
  }

  noFarella(control:FormControl):{[s: string]: boolean}{
    
    console.log(control.value);
    if(control.value?.toLowerCase()=== 'farella'){
      return{
        noFarella:true
      }
    }
    return null as any;
  }

  noMeleneses(control:FormControl):{[s: string]: boolean}{
    
    console.log(control.value);
    if(control.value?.toLowerCase()=== 'meleneses'){
      return{
        noMeleneses:true
      }
    }
    return null as any;
  }

  noPrieto(control:FormControl):{[s: string]: boolean}{
    
    console.log(control.value);
    if(control.value?.toLowerCase()=== 'prieto'){
      return{
        noPrieto:true
      }
    }
    return null as any;
  }



}
